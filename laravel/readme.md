<div align="center">
    <img src="https://img.shields.io/badge/Laravel-FF2D20?style=flat-square&logo=laravel&logoColor=white">
    <img src="https://img.shields.io/badge/npm-CB3837?style=flat-square&logo=npm&logoColor=white">
    <img src="https://img.shields.io/badge/Bootstrap-563D7C?style=flat-square&logo=bootstrap&logoColor=white">
    <img src="https://img.shields.io/badge/Vue.js-35495E?style=flat-square&logo=vuedotjs&logoColor=4FC08D">
    <img src="https://img.shields.io/badge/MySQL-00000F?style=flat-square&logo=mysql&logoColor=white">
</div>

1. Copy the example env file and make the required configuration changes in the .env file
```
cp .env.example .env
```

2. Install all the dependencies using composer

```
composer install
```

3. Generate a new application key

```
php artisan:key generate
```

4. Run the database migrations & seeders (Set the database connection in .env before migrating)

```
php artisan migrate --seed
```

5. Install your project's frontend dependencies using the Node package manager (NPM)
```
npm install
```

6. Compile frontend dependencies

```
# Development
npm run dev

# Production (minified files)
npm run prod
```

7. Create storage symlink
```
php artisan storage:link
```

8. Start Websockets server
```
php artisan websockets:serve
```

<div align="center">
<a target="_blank" href="https://gitlab.com/jagoanmamah">
<img src="https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white">
</a>
</div>


